﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Neon.Domain.Interface.Repositorio;
using Neon.Domain.Modelos;
using Neon.Infrastructure.Abstractions.Logger.Interfaces;
using Neon.Infrastructure.Abstractions.Repositories;
using Neon.Infrastructure.Configuration.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Neon.Data.Repositorio
{

    public class PottencialRepositorio : BaseRepository, IPottencialRepositorio
    {
        private readonly IConnectionStringProvider _connectionStringProvider;

        public PottencialRepositorio(ILog logger, IConfiguration configuration, IConnectionStringProvider connectionStringProvider) : base(logger, configuration.GetValue("ProjectName", ""))
        {
            _connectionStringProvider = connectionStringProvider;
        }

        public IEnumerable<Cliente> BuscarIdContaConductorCredito(params int[] idsCliente)
        {
            using (var conexao = new SqlConnection(_connectionStringProvider.GetConnectionString()))
            {
                var resposta = conexao.Query<Cliente>("select Idcliente, IdContaConductorCredito from CreditoContratoCliente where idCliente in @idsCliente", new { idsCliente });
                return resposta;
            }
        }

        public bool AtualizarLimitesCliente(int idCliente, decimal limiteGlobal, decimal limiteMaximo, decimal saldoDisponivelGlobal)
        {
            var query = @"update CreditoContratoCliente
                          set LimiteCreditoTotal = @limiteGlobal,
                              LimiteCredito = @limiteMaximo,
                              LimiteCreditoDisponivel = @saldoDisponivelGlobal
                          where IdCliente = @idCliente";

            var resposta = ExecuteAsync(connection => connection.ExecuteAsync(query, new { idCliente, limiteGlobal, limiteMaximo, saldoDisponivelGlobal }), connectionString: _connectionStringProvider.GetConnectionString(), throwError: true).GetAwaiter().GetResult();


            return resposta.Result > 0;
        }

        public bool AtualizarProstaExpirada(int id)
        {
            var query = @"update CreditLimitIncreaseProposal 
                          set ProposalStatus = 4, 
                              UPDATED_AT_DT = GETDATE(), 
                              UPDATED_BY_DS = 'Neon.Creditcard.Expirar.Aumento.Limite' where Id =@id";

            var resposta = ExecuteAsync(connection => connection.ExecuteAsync(query, new { id }), connectionString: _connectionStringProvider.GetConnectionString(), throwError: true).GetAwaiter().GetResult();

            return resposta.Result > 0;
        }

        public IEnumerable<CreditoContratoClientePropostaLimite> BuscarPropostasDeAumentoDeLimiteExpiradas()
        {
            var query = @"SELECT 
                             CreditLimitIncreaseProposal.Id,
                             Idcliente, 
                             IdContaConductorCredito,
                             CreditLimitIncreaseProposal.NewLimit as NovoLimite,
                             CreditLimitIncreaseProposal.OldLimit as LimiteAntigo
                          FROM CreditoContratoCliente 
                          INNER JOIN CreditLimitIncreaseProposal ON CreditLimitIncreaseProposal.ClientId = CreditoContratoCliente.IdCliente
                          WHERE 
                              CreditLimitIncreaseProposal.ExpirationDate < '20191230' --Convert(DateTime, DATEDIFF(DAY, -1, GETDATE()))
                          and CreditLimitIncreaseProposal.ProposalStatus = 1";

            var resposta = ExecuteAsync(connection => connection.QueryAsync<CreditoContratoClientePropostaLimite>(query), connectionString: _connectionStringProvider.GetConnectionString(), throwError: true).GetAwaiter().GetResult();

            return resposta.Result;
        }


        public async Task SalvarSimulacaoBV(CreditSimulationResponse simulacao)
        {
            var query = @"INSERT INTO CreditSimulation(Document,ReferenceDate,Result,Score,SimulationCode)
                          Values(@Document, @ReferenceDate, @Result, @Score, @SimulationCode)";

            var creditSimulator = await ExecuteAsync(connection => connection.ExecuteAsync(query, new
            {
                simulacao.Document,
                ReferenceDate = DateTime.Now,
                Result = simulacao.CreditPersonSituation.Code,
                Score = simulacao.ClientNote,
                simulacao.SimulationCode
            }, commandType: System.Data.CommandType.Text), connectionString: _connectionStringProvider.GetConnectionString(), throwError: true);
        }

        public async Task<bool> TemSimulacaoNosUltimos30Dias(string document)
        {
            var sql = @" SELECT Count(1)
                         FROM CreditSimulation with(nolock)
                         WHERE document = @document
                         AND ReferenceDate >= @maxDate
                         AND Score <> -1";

            var simulations = await ExecuteAsync(connection => connection.QueryFirstOrDefaultAsync<int>(sql, new
            {
                document = new DbString() { Length = 20, IsAnsi = true, Value = document, IsFixedLength = false },
                maxDate = DateTime.Today.AddDays(30),
            }, commandType: System.Data.CommandType.Text), connectionString: _connectionStringProvider.GetConnectionString(), throwError: true);

            return simulations.Result > 0;
        }

        public async Task<List<string>> BuscarClientesPendentesDeBatidaNaBV()
        {
            var query = @"      select   
                                    cpf as document
                                FROM clientregister a
                                left join (select document, min(referencedate) referencedate from creditsimulation cs with(nolock) group by document) b on a.cpf = b.document
                                where 
                                    registerdate >= '20200716'
                                and registerdate <= '2020-07-20 20:21:00.000'
                                and ((lastversioncode in ('3.10.25','3.10.26','3.10.27') AND platform = 'Android') OR (lastversioncode = '3.10.20' AND platform = 'iOS'))
                                and b.document is null
                            union 
                                select 
                                    distinct document
                                from  CreditSimulation
                                where convert(date,referencedate) >= '2020-05-01'
                                and score =  '-1'
                                and document not in (select document from  CreditSimulation where convert(date,referencedate) >= '2020-05-02' and score <> '-1')";

            var resposta = await ExecuteAsync(connection => connection.QueryAsync<string>(query, commandTimeout: 200), connectionString: _connectionStringProvider.GetConnectionString(), throwError: true);

            return resposta.Result.ToList();
        }
    }
}