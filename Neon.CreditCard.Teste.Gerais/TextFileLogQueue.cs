using FileHelpers;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Neon.CreditCard.Teste.Gerais
{
    public class TextFileQueue : IDisposable
    {
        private readonly ConcurrentQueue<Cadastral> _queue;
        private readonly CancellationTokenSource _consumerCancellationTokenSource;
        private readonly string _logFilePath;

        private bool _disposed;

        public TextFileQueue(string logFilePath)
        {
            _queue = new ConcurrentQueue<Cadastral>();
            _consumerCancellationTokenSource = new CancellationTokenSource();
            _logFilePath = logFilePath;

            ConsumeQueue();
        }

        public void Publish(Cadastral cadastral)
        {
            _queue.Enqueue(cadastral);
        }

        public void ConsumeQueue()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    _consumerCancellationTokenSource.Token.ThrowIfCancellationRequested();

                    if (_queue.TryDequeue(out Cadastral cadastral))
                    {
                        try
                        {
                            var engine = new FileHelperEngine<Cadastral>();

                            engine.AppendToFile(_logFilePath, cadastral);
                        }
                        catch
                        {
                            // Propositalmente suprimido, logs não devem interromper o aplicativo com seus erros
                        }
                    }
                }
            }, _consumerCancellationTokenSource.Token);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                try
                {
                    _consumerCancellationTokenSource.Cancel();
                }
                catch
                {
                    // Propositalmente suprimido, logs não devem interromper o aplicativo com seus erros
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~TextFileQueue()
        {
            Dispose(false);
        }
    }
}
