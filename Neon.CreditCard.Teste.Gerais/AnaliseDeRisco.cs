﻿using Microsoft.Extensions.DependencyInjection;
using Neon.Domain.Helper;
using Neon.Domain.Interface.Acl;
using Neon.Infra.IoC;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Neon.CreditCard.Teste.Gerais
{
    public class AnaliseDeRisco
    {
        private static Stopwatch _cronometro;

        private static IRiskAnalyzesAcl _riskAnalyzesAcl;
        private  void CarregarDependencias()
        {
            var serviceProvider = DependencyInjection.RegisterServices();
            _riskAnalyzesAcl = serviceProvider.GetService<IRiskAnalyzesAcl>();
        }

        public async Task Run()
        {
            CarregarDependencias();

            Console.WriteLine("Iniciando...");
            _cronometro = Stopwatch.StartNew();

            await Executar();

            _cronometro.Stop();
            var timeSpan = new TimeSpan(_cronometro.ElapsedTicks);
            Console.WriteLine($"\nTempo decorrido: {timeSpan.Hours}h {timeSpan.Minutes}m {timeSpan.Seconds}s {timeSpan.Milliseconds}ms");

            Console.ReadKey();
        }

        private async Task Executar()
        {
            var idsCliente = File.ReadAllLines("Results.csv").Select(x => int.Parse(x));

            await idsCliente.ForEachAsyncConcurrent(async idCliente =>
            {
                var simulacao = await _riskAnalyzesAcl.IsAFraudsterClient(new Domain.Modelos.RiskAnalyzesAclRequest(idCliente));
                Console.WriteLine(JsonSerializer.Serialize(simulacao));

            }, 15);
        }
    }
}
