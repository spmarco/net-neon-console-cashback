﻿using AutoFixture;
using FileHelpers;
using Neon.Domain.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Neon.CreditCard.Teste.Gerais
{
    class Program
    {
        private static Stopwatch _cronometro;
        
        public static async Task Main(string[] args)
        {
            var fixture = new  Fixture();
            var textQueue = new TextFileQueue("cadastral.txt");

            var count = 0;

            var baseCadastral = new List<Cadastral>();

            Console.WriteLine("Inicio");
            _cronometro = Stopwatch.StartNew();
            
            while (count < int.Parse(args.First()))
            {
                Console.WriteLine(count);
                var item = fixture.Build<Cadastral>()
                                  .With(x => x.Cpf, count.ToString().PadLeft(11, '0'))
                                  .With(x => x.DataDeNascimento, fixture.Create<DateTime>().ToString("dd/MM/yyyy"))
                                  .With(x => x.RendaPresumida, fixture.Create<int>().ToString())

                                  .With(x => x.Nome, $"Nome-{fixture.Create<string>()}")
                                  .With(x => x.NomeMae, $"NomeMae-{fixture.Create<string>()}")
                                  .With(x => x.SituacaoCadastral, $"SituacaoCadastral-{fixture.Create<string>()}")
                                  .With(x => x.Naturalidade, $"Naturalidade-{fixture.Create<string>()}")
                                  .With(x => x.NomeConjuge, $"NomeConjuge-{fixture.Create<string>()}")
                                  .Create();

                baseCadastral.Add(item);
                count++;
            }


            foreach (var item in baseCadastral)
            {
                File.AppendAllLines("cadastral.txt", new[] { $"{item.Cpf };{item.Nome };{item.DataDeNascimento };{item.NomeMae};{item.SituacaoCadastral};{item.Naturalidade};{item.UF};{item.NomeConjuge};{item.RendaPresumida};" });



                //var engine = new FileHelperEngine<Cadastral>();
                //engine.AppendToFile("cadastral.txt", item);
            }


            //await baseCadastral.ForEachAsyncConcurrent(async cadastral =>
            //{
            //    var engine = new FileHelperEngine<Cadastral>();
            //    engine.AppendToFile("cadastral.txt", cadastral);
            //    //textQueue.Publish(cadastral);
            //}, 15);

            _cronometro.Stop();
            var timeSpan = new TimeSpan(_cronometro.ElapsedTicks);
            Console.WriteLine($"\nTempo decorrido: {timeSpan.Hours}h {timeSpan.Minutes}m {timeSpan.Seconds}s {timeSpan.Milliseconds}ms");

        }
    }
}
