﻿using FileHelpers;
using System.ComponentModel.DataAnnotations;

namespace Neon.CreditCard.Teste.Gerais
{
    [FixedLengthRecord()]
    public class Cadastral
    {
        [StringLength(11)]
        [FieldFixedLength(11)]
        public string Cpf { get; set; }
        [StringLength(255)]
        [FieldFixedLength(255)]
        public string Nome { get; set; }
        [StringLength(20)]
        [FieldFixedLength(20)]
        public string DataDeNascimento { get; set; }
        [StringLength(60)]
        [FieldFixedLength(60)]
        public string NomeMae { get; set; }
        [StringLength(40)]
        [FieldFixedLength(40)]
        public string SituacaoCadastral { get; set; }
        [StringLength(255)]
        [FieldFixedLength(255)]
        public string Naturalidade { get; set; }
        [StringLength(2)]
        [FieldFixedLength(2)]
        public string UF { get; set; }
        [StringLength(255)]
        [FieldFixedLength(255)]
        public string NomeConjuge { get; set; }
        [StringLength(4)]
        [FieldFixedLength(4)]
        public string RendaPresumida { get; set; }
    }
}
