﻿using Microsoft.Extensions.DependencyInjection;
using Neon.Domain;
using Neon.Domain.Interface.Negocio;
using Neon.Infra.IoC;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Neon.Credicard.Request.Credit.Bv
{
    class Program
    {
        private static Stopwatch _cronometro;
        private static IAnaliseDeCreditoBv _analiseDeCreditoBv;

        public static async Task Main(string[] args)
        {
            //if (!ValidarEntrada(args))
            //    return;

            CarregarDependencias();

            Console.WriteLine("Iniciando...");
            _cronometro = Stopwatch.StartNew();
            var salvarNaCreditSimulation = true;
            Parametros.CAMINHO_ARQUIVO = "Analise_Bv.csv";

            await _analiseDeCreditoBv.Executar(salvarNaCreditSimulation);

            _cronometro.Stop();
            var timeSpan = new TimeSpan(_cronometro.ElapsedTicks);
            Console.WriteLine($"\nTempo decorrido: {timeSpan.Hours}h {timeSpan.Minutes}m {timeSpan.Seconds}s {timeSpan.Milliseconds}ms");
            Console.WriteLine("Fim");

            Console.ReadKey();
        }

        private static void CarregarDependencias()
        {
            var serviceProvider = DependencyInjection.RegisterServices();
            _analiseDeCreditoBv = serviceProvider.GetService<IAnaliseDeCreditoBv>();
        }

        private static bool ValidarEntrada(string[] args)
        {
            var mensagem = "";

            if (!args.Any())
            {
                mensagem = "<caminho do arquivo .csv>";
                Console.WriteLine(mensagem);
            }

            return string.IsNullOrEmpty(mensagem);
        }
    }
}
