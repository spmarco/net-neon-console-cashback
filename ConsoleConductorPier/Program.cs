﻿using Microsoft.Extensions.DependencyInjection;
using Neon.Domain;
using Neon.Domain.Interface.Negocio;
using Neon.Infra.IoC;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace ConsoleConductorPier
{
    class Program
    {
        private static IEnumerable<string> _acoes = new[] { "DEBITOCONTANEON", "CASHBACK", "ATIVARCONTA", "AUMENTODELIMITE" };
        private static Stopwatch _cronometro;
        public static IEnviarAjusteConductorPier _enviarAjuste;
        public static IReativarClienteConductorPier _reativarClienteConductor;
        public static IAumentoDeLimiteClienteNeon _aumentoDeLimiteClienteNeon;

        public static void Main(string[] args)
        {
            if (!ValidarEntrada(args))
                return;

            CarregarDependencias();

            Console.WriteLine("Iniciando...");
            _cronometro = Stopwatch.StartNew();

            Parametros.ACAO = args[0];
            Parametros.EH_PRA_VALER = bool.Parse(args[1]);
            if (args[0] != "CONTANEON")
                Parametros.CAMINHO_ARQUIVO = args[2];

            switch (Parametros.ACAO)
            {
                case "DEBITOCONTANEON":
                    DebitoContaNeon(args[2]);
                    break;
                case "CASHBACK":
                    _enviarAjuste.Executar();
                    break;
                case "ATIVARCONTA":
                    _reativarClienteConductor.Executar();
                    break;
                case "AUMENTODELIMITE":
                    _aumentoDeLimiteClienteNeon.Executar();
                    break;

            }

            _cronometro.Stop();
            var timeSpan = new TimeSpan(_cronometro.ElapsedTicks);
            Console.WriteLine($"\nTempo decorrido: {timeSpan.Hours}h {timeSpan.Minutes}m {timeSpan.Seconds}s {timeSpan.Milliseconds}ms");

            Console.ReadKey();
        }

        private static void CarregarDependencias()
        {
            var serviceProvider = DependencyInjection.RegisterServices();
            _enviarAjuste = serviceProvider.GetService<IEnviarAjusteConductorPier>();
            _reativarClienteConductor = serviceProvider.GetService<IReativarClienteConductorPier>();
            _aumentoDeLimiteClienteNeon = serviceProvider.GetService<IAumentoDeLimiteClienteNeon>();
        }

        private static void DebitoContaNeon(string valor)
        {
            _enviarAjuste.DefinirValorDebitoNeon(decimal.Parse(valor, NumberStyles.AllowDecimalPoint, new CultureInfo("pt-BR")));
            _enviarAjuste.FazerLancamentoDebitoNeon();
        }

        private static bool ValidarEntrada(string[] args)
        {
            var mensagem = "";

            if (!args.Any() || args.Count() != 3 || !_acoes.Contains(args[0]))
            {
                mensagem = "Valor inválido - Chamada correta => <acoes[DEBITOCONTANEON, ATIVARCONTA, CASHBACK, AUMENTODELIMITE]>, <true/false -> teste ou producao> <caminho do arquivo .csv>";
                Console.WriteLine(mensagem);
            }

            return string.IsNullOrEmpty(mensagem);
        }
    }
}
