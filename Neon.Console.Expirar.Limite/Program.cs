﻿using Microsoft.Extensions.DependencyInjection;
using Neon.Domain.Negocio;
using Neon.Infra.IoC;
using System;
using System.Linq;

namespace Neon.Creditcard.Expirar.Aumento.Limite
{
    class Program
    {
        public static ExpirarPropostaDeAumentoDeLimite _expirarLimite;

        static void Main(string[] args)
        {
            Console.WriteLine("Inicio Console");
            CarregarDependencias();
            _expirarLimite.Executar(args.Select(x => int.Parse(x)));
        }


        private static void CarregarDependencias()
        {
            var serviceProvider = DependencyInjection.RegisterServices();
            _expirarLimite = serviceProvider.GetService<ExpirarPropostaDeAumentoDeLimite>();
        }
    }
}
