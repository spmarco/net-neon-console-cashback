﻿using System.Net.Http;

namespace Neon.Infra.FlurlClientFactory
{
    public class GzipClientFactory : Flurl.Http.Configuration.IHttpClientFactory
    {
        public HttpMessageHandler CreateMessageHandler() => new HttpClientHandler()
        {
            AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate
        };

        public HttpClient CreateHttpClient(HttpMessageHandler handler) => new HttpClient(handler);
    }
}
