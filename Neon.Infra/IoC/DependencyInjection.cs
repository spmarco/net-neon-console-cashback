﻿using Flurl.Http;
using Flurl.Http.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Neon.Data.Repositorio;
using Neon.Domain.ACL;
using Neon.Domain.Helper;
using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Helper;
using Neon.Domain.Interface.Negocio;
using Neon.Domain.Interface.Negocio.HttpCall;
using Neon.Domain.Interface.Repositorio;
using Neon.Domain.Modelos;
using Neon.Domain.Negocio;
using Neon.Domain.Negocio.HttpCall;
using Neon.Infra.FlurlClientFactory;
using Neon.Infrastructure.Abstractions.Statistics.Interfaces;
using Neon.Infrastructure.Configuration.IoC;
using System;
using System.IO;
using System.Net.Http;

namespace Neon.Infra.IoC
{
    public static class DependencyInjection
    {
        public static IServiceProvider ServiceProvider { get; private set; }

        public static IServiceProvider RegisterServices()
        {
            if (ServiceProvider == null)
            {
                var serviceCollection = new ServiceCollection();

                FlurlHttp.Configure(settings =>
                {
                    settings.HttpClientFactory = new ProxyHttpClientFactory();
                    settings.Timeout = new TimeSpan(0, 0, 120);
                    settings.HttpClientFactory = new GzipClientFactory();
                });

                ServiceProvider = serviceCollection.ConfigureServices()
                                                   .RegisterDependencies()
                                                   .BuildServiceProvider();
            }

            return ServiceProvider;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                                          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                                                          .AddEnvironmentVariables()
                                                          .Build();

            var showConsoleLog = configuration.GetValue("ShowConsoleLog", true);

            services.AddSingleton(typeof(IConfiguration), configuration)
                    .RegisterNeonConfiguration(configuration)
                    .RegisterNeonLog(showConsoleLog)
                    .RegisterNeonStatistic(showConsoleLog);

            return services;
        }

        private static IServiceCollection RegisterDependencies(this IServiceCollection services)
        {
            services.AddSingleton<IEnviarAjusteConductorPier, EnviarAjusteConductorPier>(x => new EnviarAjusteConductorPier(int.Parse(x.GetService<IConfiguration>().GetSection("NeonBankAccountId").Value), x.GetService<IStatisticProvider>(), x.GetService<IConductorPierACL>(), x.GetService<ILancamentoDiarioAcl>(), x.GetService<IPottencialRepositorio>()));
            services.AddSingleton<IReativarClienteConductorPier, ReativarClienteConductorPier>();
            services.AddSingleton<IAumentoDeLimiteClienteNeon, AumentoDeLimiteClienteNeon>();
            services.AddScoped<ExpirarPropostaDeAumentoDeLimite, ExpirarPropostaDeAumentoDeLimite>();
            services.AddScoped<IAnaliseDeCreditoBv, AnaliseDeCreditoBv>();

            services.AddScoped<IConductorPierACL, ConductorPierACL>();
            services.AddScoped<ILancamentoDiarioAcl, LancamentoDiarioAcl>();
            services.AddScoped<IRiskAnalyzesAcl, RiskAnalyzesAcl>();
            services.AddScoped<IBvAcl, BvAcl>();
            
            services.AddScoped<IPottencialRepositorio, PottencialRepositorio>();
            services.AddScoped<IHttpCallBase, HttpCallBase>();

            services.AddScoped<ICache<OAuthResponse>, Cache<OAuthResponse>>();
            services.AddScoped<ICache<CreditSimulationResponse>, Cache<CreditSimulationResponse>>();

            return services;
        }

        private class ProxyHttpClientFactory : DefaultHttpClientFactory
        {
            public override HttpMessageHandler CreateMessageHandler() => new HttpClientHandler { UseDefaultCredentials = true };
        }
    }
}
