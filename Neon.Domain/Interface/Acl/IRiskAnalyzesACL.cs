﻿using Neon.Domain.Modelos;
using System.Threading.Tasks;

namespace Neon.Domain.Interface.Acl
{
    public interface IRiskAnalyzesAcl
    {
        Task<RiskAnalyzesAclResponse> IsAFraudsterClient(RiskAnalyzesAclRequest request);
    }
}
