﻿using Neon.Domain.Modelos;

namespace Neon.Domain.Interface.Acl
{
    public interface ILancamentoDiarioAcl
    {
        LancamentoDiarioRequisicao InserirLancamentoContaCorrente(LancamentoDiarioRequisicao requisicao);
    }
}