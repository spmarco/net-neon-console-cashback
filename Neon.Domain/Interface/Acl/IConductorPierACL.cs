﻿using Neon.Domain.Modelos;

namespace Neon.Domain.Interface.Acl
{
    public interface IConductorPierACL
    {
        ConductorRespostaAjuste EnviarAjusteFinanceiro(ConductorRequisicaoAjuste requisicao);
        bool AtivarContaCredito(int idContaConductorCredito);
        Paginacao<Transacao> ObterTransacoesProcessadas(int idContaConductorCredito);
        ConductorLimiteResposta AlterarLImiteGlobalMaximo(int idContaConductorCredito, decimal limiteGlobal, decimal limiteMaximo);
        ConductorLimiteResposta AlterarLImiteGlobal(int idContaConductorCredito, decimal limiteGlobal);
        ConductorLimiteResposta ObterLimites(int idContaConductorCredito);
    }
}