﻿using Neon.Domain.Modelos;
using System.Threading.Tasks;

namespace Neon.Domain.Interface.Acl
{
    public interface IBvAcl
    {
        Task<CreditSimulationResponse> RequestCreditSimulation(string document);
        Task<CreditSimulationResponse> RequestCreditSimulationRetry(string document);
        OAuthResponse Authenticate();
    }
}
