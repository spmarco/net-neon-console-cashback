﻿namespace Neon.Domain.Interface.Negocio
{
    public interface IEnviarAjusteConductorPier
    {
        void Executar();
        void FazerLancamentoDebitoNeon();
        void DefinirValorDebitoNeon(decimal valor);
    }
}