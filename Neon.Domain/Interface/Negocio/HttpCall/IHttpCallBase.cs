﻿using Flurl;
using Flurl.Http;
using Neon.Domain.Negocio.HttpCall;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Neon.Domain.Interface.Negocio.HttpCall
{
    public interface IHttpCallBase
    {
        Task<HttpCallResult<T>> CallHttpWithHeader<T>(Func<IFlurlRequest, Task<T>> httpCallAction,
                                                      string baseUrl,
                                                      string suffixUrl,
                                                      Dictionary<string, string> header,
                                                      bool useDefaultCredentials = true,
                                                      int clientId = 0,
                                                      bool throwException = false,
                                                      [CallerMemberName] string callerMemberName = "");

        Task<HttpCallResult<T>> CallHttp<T>(Func<Url, Task<T>> httpCallAction,
                                                 string baseUrl,
                                                 string suffixUrl,
                                                 int clientId = 0,
                                                 bool throwException = false,
                                                 [CallerMemberName] string callerMemberName = "");
    }
}
