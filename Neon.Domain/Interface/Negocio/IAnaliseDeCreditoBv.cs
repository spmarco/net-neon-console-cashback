﻿using System.Threading.Tasks;

namespace Neon.Domain.Interface.Negocio
{
    public interface IAnaliseDeCreditoBv
    {
        Task Executar(bool salvarNaCreditSimulation);
    }
}