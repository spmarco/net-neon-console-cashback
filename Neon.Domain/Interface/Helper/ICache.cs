﻿using Neon.Domain.Helper;

namespace Neon.Domain.Interface.Helper
{
    public interface ICache<T>
    {
        T Get(string key, TypeCache typeCache = TypeCache.MemoryCache);
        void Set(string key, T value, int expiresIn = 3600, TypeCache typeCache = TypeCache.MemoryCache);
        void Clear();
    }
}
