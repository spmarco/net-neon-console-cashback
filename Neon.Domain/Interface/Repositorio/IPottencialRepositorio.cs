﻿using Neon.Domain.Modelos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Neon.Domain.Interface.Repositorio
{
    public interface IPottencialRepositorio
    {
        IEnumerable<Cliente> BuscarIdContaConductorCredito(params int[] idsCliente);
        IEnumerable<CreditoContratoClientePropostaLimite> BuscarPropostasDeAumentoDeLimiteExpiradas();
        bool AtualizarLimitesCliente(int idCliente, decimal limiteGlobal, decimal limiteMaximo, decimal saldoDisponivelGlobal);
        bool AtualizarProstaExpirada(int id);
        Task SalvarSimulacaoBV(CreditSimulationResponse simulacao);
        Task<bool> TemSimulacaoNosUltimos30Dias(string document);
        Task<List<string>> BuscarClientesPendentesDeBatidaNaBV();
    }
}