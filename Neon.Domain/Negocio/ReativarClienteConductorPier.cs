﻿using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Negocio;
using Neon.Infrastructure.Abstractions.Logger.Entities;
using Neon.Infrastructure.Abstractions.Logger.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Neon.Domain.Negocio
{
    public class ReativarClienteConductorPier : IReativarClienteConductorPier
    {
        private readonly ILog _logger;
        private readonly IConductorPierACL _conductorPierACL;

        public ReativarClienteConductorPier(ILog logger, IConductorPierACL conductorPierACL)
        {
            _logger = logger;
            _conductorPierACL = conductorPierACL;
        }

        public void Executar()
        {
            try
            {
                var idsConductor = CarregarIdsConductor();

                foreach (var idConductor in idsConductor)
                {
                    if (DaParaReativar(idConductor) && _conductorPierACL.AtivarContaCredito(idConductor))
                    {
                        LogTxt(JsonConvert.SerializeObject(new { idConductor, mensagem = "conta reativada com sucesso" }));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(new LogEntity { Exception = ex }, new { message = "Erro ao carregar os ids" });
                throw;
            }
        }

        private bool DaParaReativar(int idConductor)
        {
            var transacoes = _conductorPierACL.ObterTransacoesProcessadas(idConductor);

            if (transacoes == null || transacoes.content == null)
            {
                LogTxt(JsonConvert.SerializeObject(new { idConductor, mensagem = "Esta sem saldo... ok" }));
                return true;
            }

            if (transacoes.content.Any())
                LogTxt(JsonConvert.SerializeObject(new { idConductor, mensagem = "o cliente esta com transação mesmo com conta bloqueada" }));

            return !transacoes.content.Any();
        }

        private IEnumerable<int> CarregarIdsConductor()
        {
            var lista = new List<int>();
            var linhas = File.ReadAllLines(Parametros.CAMINHO_ARQUIVO);

            for (int i = 0; i < linhas.Length; i++)
            {
                var valores = linhas[i].Split(';');
                lista.Add(int.Parse(valores[1]));
            }

            return lista;
        }

        private void LogTxt(string linha)
        {
            if (!Parametros.EH_PRA_VALER)
                return;

            _logger.Info(new LogEntity(), new { linha = linha });
            File.AppendAllLines(Parametros.CAMINHO_ARQUIVO_SUCESSO, new[] { linha });
        }
    }
}
