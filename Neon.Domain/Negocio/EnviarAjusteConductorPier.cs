﻿using Neon.Domain.Helper;
using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Negocio;
using Neon.Domain.Interface.Repositorio;
using Neon.Domain.Modelos;
using Neon.Infrastructure.Abstractions.Statistics.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Neon.Domain.Negocio
{
    public class EnviarAjusteConductorPier : IEnviarAjusteConductorPier
    {
        private readonly int _idContaBancariaNeon;
        private readonly IStatisticProvider _statisticProvider;
        private readonly IConductorPierACL _conductorPierACL;
        private readonly ILancamentoDiarioAcl _lancamentoDiarioAcl;
        private readonly IPottencialRepositorio _pottencialRepositorio;
        private decimal _valorDebitoNeon;

        public EnviarAjusteConductorPier(int idContaBancariaNeon, IStatisticProvider statisticProvider, IConductorPierACL conductorPierACL, ILancamentoDiarioAcl lancamentoDiarioAcl, IPottencialRepositorio pottencialRepositorio)
        {
            _idContaBancariaNeon = idContaBancariaNeon;
            _statisticProvider = statisticProvider;
            _conductorPierACL = conductorPierACL;
            _lancamentoDiarioAcl = lancamentoDiarioAcl;
            _pottencialRepositorio = pottencialRepositorio;
        }

        public void Executar()
        {
            try
            {
                var ajustes = CarregarAjustes();

                foreach (var ajuste in ajustes)
                {
                    FazerAjuste(ajuste);
                }

                FazerLancamentoDebitoNeon();
            }
            catch (Exception ex)
            {
                if (ex is FormatException)
                    Console.WriteLine("Valores inválidos, formato correto pt-BR");
                else
                    Console.WriteLine(ex.Message);

                throw;
            }
        }

        public void FazerLancamentoDebitoNeon()
        {
            if (_valorDebitoNeon == 0)
                return;

            var requisicao = new LancamentoDiarioCreditoRequisicao(idContaBancaria: _idContaBancariaNeon,
                                                                   tipoOperacao: "DEBITO",
                                                                   tipoLancamento: "CONDUCTOR",
                                                                   valor: _valorDebitoNeon,
                                                                   idCategoria: 1,
                                                                   dataTransacao: DateTime.Today,
                                                                   dataCadastro: DateTime.Today,
                                                                   descricao: "Pagamentos de cashback",
                                                                   uniqueId: "0",
                                                                   aplicar: "AMBOS",
                                                                   status: 1);

            _lancamentoDiarioAcl.InserirLancamentoContaCorrente(requisicao);
        }

        public void DefinirValorDebitoNeon(decimal valor)
        {
            _valorDebitoNeon = valor;
        }

        private void FazerAjuste(AjusteNeon ajuste)
        {
            try
            {
                var resposta = _conductorPierACL.EnviarAjusteFinanceiro(ajuste.RequisicaoAjuste);

                if (resposta != null && resposta.Id > 0)
                    _valorDebitoNeon += resposta.Valor;

                LogTxtAjuste(JsonConvert.SerializeObject(new { ajuste.IdCliente, resposta }));
            }
            catch (Exception ex)
            {
                LogTxtAjuste(JsonConvert.SerializeObject(new { erro = ex.ToString(), ajuste }));
            }
        }

        private void LogTxtAjuste(string linha)
        {
            if (!Parametros.EH_PRA_VALER)
                return;

            _statisticProvider.CountEvent("LogTxtAjuste", linha, "sucess");
            File.AppendAllLines(Parametros.CAMINHO_ARQUIVO_SUCESSO, new[] { linha });
        }

        private IEnumerable<AjusteNeon> CarregarAjustes()
        {
            var lista = new List<AjusteNeon>();
            var linhas = File.ReadAllLines(Parametros.CAMINHO_ARQUIVO);

            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Client"))
                    continue;

                var valores = linhas[i].Split(';');

                var ajuste = new AjusteNeon
                {
                    IdCliente = int.Parse(valores[0]),
                    RequisicaoAjuste = new ConductorRequisicaoAjuste
                    {
                        ValorAjuste = decimal.Parse(valores[1], NumberStyles.AllowDecimalPoint, new CultureInfo("pt-BR")),
                        IdTipoAjuste = valores.Length > 2 ? (TipoAjusteConductor)int.Parse(valores[2]) : TipoAjusteConductor.CASHBACK
                    }
                };

                lista.Add(ajuste);
            }

            IncluirIdContaConductor(lista);

            return lista;
        }

        private void IncluirIdContaConductor(List<AjusteNeon> ajustes)
        {
            var clientesComIdContaConductor = PAginarConsulta(ajustes);

            foreach (var ajuste in ajustes)
                ajuste.RequisicaoAjuste.IdContaConductor = clientesComIdContaConductor.FirstOrDefault(x => x.Idcliente == ajuste.IdCliente).IdContaConductorCredito;
        }

        private List<Cliente> PAginarConsulta(List<AjusteNeon> ajustes)
        {
            var clientesComIdContaConductor = new List<Cliente>();
            var paginas = ajustes.Count / 2000;

            if (ajustes.Count % 2000 != 0)
                paginas++;

            for (var i = 1; i <= (paginas < 0 ? 1 : paginas); i++)
            {
                var ids = ajustes.Page(i, 2000);
                clientesComIdContaConductor.AddRange(_pottencialRepositorio.BuscarIdContaConductorCredito(ids.Select(x => x.IdCliente).ToArray()));
            }

            return clientesComIdContaConductor;
        }

        private IEnumerable<AjusteNeon> ObterAjustesDaResposta()
        {
            var lista = new List<AjusteNeon>();

            foreach (var linha in File.ReadAllLines(Parametros.CAMINHO_ARQUIVO))
            {
                var respostaConductor = JsonConvert.DeserializeObject<AjusteNeonResposta>(linha);

                lista.Add(new AjusteNeon
                {
                    IdCliente = respostaConductor.IdCliente,
                    RequisicaoAjuste = new ConductorRequisicaoAjuste
                    {
                        IdContaConductor = respostaConductor.Resposta.IdConta,
                        DataAjuste = respostaConductor.Resposta.DataAjuste,
                        IdTipoAjuste = TipoAjusteConductor.ESTORNO_CASHBACK,
                        ValorAjuste = respostaConductor.Resposta.Valor
                    }
                });
            }

            return lista;
        }

        private IEnumerable<AjusteNeon> ObterAjustesJson()
        {
            var lista = new List<AjusteNeon>();

            foreach (var linha in File.ReadAllLines(Parametros.CAMINHO_ARQUIVO))
            {
                lista.Add(JsonConvert.DeserializeObject<AjusteNeon>(linha));
            }

            return lista;
        }
    }
}
