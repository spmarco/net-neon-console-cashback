﻿using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Repositorio;
using Neon.Infrastructure.Abstractions.Statistics.Interfaces;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Neon.Domain.Negocio
{
    public class ExpirarPropostaDeAumentoDeLimite
    {
        private readonly IConductorPierACL _conductorPierACL;
        private readonly IPottencialRepositorio _pottencialRepositorio;
        private readonly IStatisticProvider _statisticProvider;

        public ExpirarPropostaDeAumentoDeLimite(IConductorPierACL conductorPierACL, IPottencialRepositorio pottencialRepositorio, IStatisticProvider statisticProvider)
        {
            _conductorPierACL = conductorPierACL;
            _pottencialRepositorio = pottencialRepositorio;
            _statisticProvider = statisticProvider;
        }


        public void Executar(IEnumerable<int> idsCliente)
        {
            var propostasExpiradas = _pottencialRepositorio.BuscarPropostasDeAumentoDeLimiteExpiradas();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            foreach (var proposta in idsCliente.Any() ? propostasExpiradas.Where(x => idsCliente.Contains(x.IdCliente)) : propostasExpiradas)
            {
                try
                {
                    Expirar(stopwatch, proposta);
                }
                catch (System.Exception)
                {
                    _statisticProvider.CountEvent("neon.creditcard.expirar.aumento.limite.end", proposta.IdCliente.ToString(), "fail", elapsedMilliseconds: stopwatch.ElapsedMilliseconds);
                }
            }

            stopwatch.Stop();
        }

        private void Expirar(Stopwatch stopwatch, Modelos.CreditoContratoClientePropostaLimite proposta)
        {
            _statisticProvider.CountEvent("neon.creditcard.expirar.aumento.limite.start", proposta.IdCliente.ToString(), "start");

            var limitesCdt = _conductorPierACL.ObterLimites(proposta.IdContaConductorCredito);

            if (limitesCdt == null)
                return;

            var novoLimiteMaximo = limitesCdt.LimiteMaximo > proposta.LimiteAntigo ? proposta.LimiteAntigo : limitesCdt.LimiteMaximo.Value;

            var respostaLimiteCdt = _conductorPierACL.AlterarLImiteGlobalMaximo(proposta.IdContaConductorCredito, proposta.LimiteAntigo, novoLimiteMaximo);

            if (respostaLimiteCdt.Id != 0)
            {
                if (_pottencialRepositorio.AtualizarLimitesCliente(proposta.IdCliente, proposta.LimiteAntigo, novoLimiteMaximo, respostaLimiteCdt.SaldoDisponivelGlobal.Value))
                    _pottencialRepositorio.AtualizarProstaExpirada(proposta.Id);
            }

            _statisticProvider.CountEvent("neon.creditcard.expirar.aumento.limite.end", proposta.IdCliente.ToString(), "success", elapsedMilliseconds: stopwatch.ElapsedMilliseconds);
        }
    }
}
