﻿using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.Extensions.Configuration;
using Neon.Domain.Helper;
using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Negocio;
using Neon.Domain.Interface.Repositorio;
using Neon.Domain.Modelos;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Neon.Domain.Negocio
{
    public class AnaliseDeCreditoBv : IAnaliseDeCreditoBv
    {
        private readonly IBvAcl _bvAcl;
        private readonly IPottencialRepositorio _repositorio;
        private readonly string _directoryCache;

        public AnaliseDeCreditoBv(IBvAcl bvAcl, IConfiguration configuration, IPottencialRepositorio repositorio)
        {
            _bvAcl = bvAcl;
            _repositorio = repositorio;
            _directoryCache = configuration.GetValue<string>("FolderCache");
        }

        public async Task Executar(bool salvarNaCreditSimulation)
        {
            var simulacoes = new List<CreditSimulationResponse>();
            try
            {
                var cpfs = await CarregarCpfsDoBanco();

                await cpfs.ForEachAsyncConcurrent(async cpf =>
                {
                    Console.WriteLine(cpf);
                    var simulacao = await _bvAcl.RequestCreditSimulationRetry(cpf);

                    if (simulacao != null && simulacao.ClientNote != -1)
                    {
                        simulacoes.Add(simulacao);

                        if (salvarNaCreditSimulation && !await _repositorio.TemSimulacaoNosUltimos30Dias(cpf))
                            await _repositorio.SalvarSimulacaoBV(simulacao);
                    }
                }, 15);

                SalvarSimulacoes(simulacoes);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        private void SalvarSimulacoes(IEnumerable<CreditSimulationResponse> simulacoes)
        {
            var csvConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ";"
            };

            var nomeArquivo = Path.GetFileName(Parametros.CAMINHO_ARQUIVO);
            var arquivoDeResposta = $"resposta_{nomeArquivo}";
            using (var writer = new StreamWriter(Parametros.CAMINHO_ARQUIVO.Replace(nomeArquivo, arquivoDeResposta)))
            using (var csv = new CsvWriter(writer, csvConfiguration))
            {
                csv.WriteRecords(simulacoes);
            }
        }

        private IEnumerable<string> CarregarCpfs()
        {
            using (var reader = new StreamReader(Parametros.CAMINHO_ARQUIVO))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var cpfs = csv.GetRecords(new { cpf = string.Empty });
                return cpfs.Select(x => x.cpf).ToList();
            }
        }

        private async Task<List<string>> CarregarCpfsDoBanco()
        {
            var cpfs = await _repositorio.BuscarClientesPendentesDeBatidaNaBV();
            return cpfs;
        }



        private void Contador()
        {
            var loop = 0;

            while (loop < 20)
            {
                Thread.Sleep(1000);
                Console.WriteLine($"#####################---------Total {Directory.GetFiles(_directoryCache).Length}---------#####################");
            }
        }
    }
}
