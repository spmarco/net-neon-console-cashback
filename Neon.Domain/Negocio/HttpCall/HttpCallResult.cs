﻿using System;

namespace Neon.Domain.Negocio.HttpCall
{
    public class HttpCallResult<T>
    {
        public T Result { get; set; }
        public Exception Exception { get; set; }
        public bool Success { get; set; }
    }
}
