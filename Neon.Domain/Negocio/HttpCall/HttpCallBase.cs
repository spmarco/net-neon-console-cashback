﻿using Flurl;
using Flurl.Http;
using Microsoft.Extensions.Configuration;
using Neon.Domain.Interface.Negocio.HttpCall;
using Neon.Infrastructure.Abstractions.Logger.Entities;
using Neon.Infrastructure.Abstractions.Logger.Interfaces;
using Neon.Infrastructure.Abstractions.Statistics.Interfaces;
using Polly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Neon.Domain.Negocio.HttpCall
{
    public class HttpCallBase : IHttpCallBase
    {
        protected readonly ILog _logger;
        protected readonly IStatisticProvider _statisticProvider;
        private readonly IConfiguration _configuration;

        public HttpCallBase(ILog logger, IStatisticProvider statisticProvider, IConfiguration configuration)
        {
            _logger = logger;
            _statisticProvider = statisticProvider;
            _configuration = configuration;
        }

        public Task<HttpCallResult<T>> CallHttpWithHeader<T>(Func<IFlurlRequest, Task<T>> httpCallAction, string baseUrl, string suffixUrl, Dictionary<string, string> header, bool useDefaultCredentials = true, int clientId = 0, bool throwException = false, [CallerMemberName] string callerMemberName = "")
        {
            return CallHttp((url) =>
            {
                if (header == null)
                {
                    header = new Dictionary<string, string>();
                }

                if (useDefaultCredentials)
                {
                    var fc = new FlurlClient();
                    ((HttpClientHandler)fc.HttpMessageHandler).UseDefaultCredentials = true;
                    url.WithClient(fc);
                }

                return httpCallAction(url.WithHeaders(header));
            },
            baseUrl,
            suffixUrl,
            clientId,
            throwException, callerMemberName);
        }

        public async Task<HttpCallResult<T>> CallHttp<T>(Func<Url, Task<T>> httpCallAction,
                                                 string baseUrl,
                                                 string suffixUrl,
                                                 int clientId = 0,
                                                 bool throwException = false,
                                                 [CallerMemberName] string callerMemberName = "")
        {
            var logEntity = Build(clientId, methodSource: callerMemberName);

            baseUrl = baseUrl == null || baseUrl.EndsWith("/") ? baseUrl : $"{baseUrl}/";
            if (string.IsNullOrEmpty(baseUrl) || baseUrl.Equals("/") || string.IsNullOrEmpty(suffixUrl))
            {
                _logger.Error(logEntity, new { BaseUrl = baseUrl, SuffixUrl = suffixUrl, Message = "Invalid baseUrl or sufixUrl" });
                return new HttpCallResult<T> { Success = false };
            }

            return await CallWithRetry(httpCallAction, baseUrl, suffixUrl, throwException, logEntity);
        }

        private async Task<HttpCallResult<T>> CallWithRetry<T>(Func<Url, Task<T>> httpCallAction, string baseUrl, string suffixUrl, bool throwException, LogEntity logEntity)
        {
            var stopwatch = new Stopwatch();
            try
            {
                return await Policy.Handle<Exception>()
                                   .WaitAndRetryAsync(retryCount: 3, retryAttempt => TimeSpan.FromSeconds(int.Parse(_configuration["SecondsRetryAttemptCallHttp"] ?? "30")),
                                   (exception, timeSpan, retryCount, context) =>
                                   {
                                       _logger.Error(logEntity, new { BaseUrl = baseUrl, SuffixUrl = suffixUrl, Message = $"Error to call url, retry {retryCount} times", ExceptionMessage = exception.Message });
                                   })
                                   .ExecuteAsync(async () => await Call(httpCallAction, baseUrl, suffixUrl, throwException, logEntity, stopwatch));
            }
            catch (Exception ex)
            {
                _statisticProvider.CountEvent("http.call.base.end", $"{baseUrl}{suffixUrl}", "fail", elapsedMilliseconds: stopwatch.ElapsedMilliseconds);

                logEntity.Exception = ex;
                _logger.Error(logEntity, new { BaseUrl = baseUrl, SuffixUrl = suffixUrl, ex.Message });

                if (throwException)
                {
                    throw;
                }

                return new HttpCallResult<T>
                {
                    Success = false,
                    Exception = ex
                };
            }
        }

        private async Task<HttpCallResult<T>> Call<T>(Func<Url, Task<T>> httpCallAction, string baseUrl, string suffixUrl, bool throwException, LogEntity logEntity, Stopwatch stopwatch)
        {
            suffixUrl = suffixUrl.StartsWith("/") ? suffixUrl.Substring(1) : suffixUrl;

            var url = baseUrl.AppendPathSegment(suffixUrl);

            stopwatch.Start();

            _statisticProvider.CountEvent("http.call.base.begin", $"{baseUrl}{suffixUrl}");

            var result = await httpCallAction(url);

            _statisticProvider.CountEvent("http.call.base.end", $"{baseUrl}{suffixUrl}", "success", elapsedMilliseconds: stopwatch.ElapsedMilliseconds);

            return new HttpCallResult<T> { Result = result, Success = true };
        }

        private LogEntity Build(int clientId = 0, Exception exception = null, [CallerMemberName]string methodSource = "")
        {
            return new LogEntity
            {
                ClientId = clientId,
                Project = _configuration["ProjectName"],
                Source = methodSource,
                Exception = exception
            };
        }
    }
}