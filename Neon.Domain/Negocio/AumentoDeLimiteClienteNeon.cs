﻿using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Negocio;
using Neon.Domain.Interface.Repositorio;
using Neon.Domain.Modelos;
using Neon.Infrastructure.Abstractions.Logger.Entities;
using Neon.Infrastructure.Abstractions.Logger.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Neon.Domain.Negocio
{
    public class AumentoDeLimiteClienteNeon : IAumentoDeLimiteClienteNeon
    {
        private readonly ILog _logger;
        private readonly IPottencialRepositorio _pottencialRepositorio;
        private readonly IConductorPierACL _conductorPierACL;


        public AumentoDeLimiteClienteNeon(ILog logger, IPottencialRepositorio pottencialRepositorio, IConductorPierACL conductorPierACL)
        {
            _logger = logger;
            _pottencialRepositorio = pottencialRepositorio;
            _conductorPierACL = conductorPierACL;
        }

        public void Executar()
        {
            try
            {
                var aumentosDeLimite = CarregarAumentos();

                foreach (var aumento in aumentosDeLimite)
                {
                    var respostaLimiteCdt = _conductorPierACL.AlterarLImiteGlobal(aumento.RequisicaoAumento.IdContaConductorCredito, aumento.RequisicaoAumento.NovoValorGlobal);

                    if (respostaLimiteCdt.Id != 0)
                    {
                        _pottencialRepositorio.AtualizarLimitesCliente(aumento.IdCliente, aumento.RequisicaoAumento.NovoValorGlobal, respostaLimiteCdt.LimiteMaximo ?? 0, respostaLimiteCdt.SaldoDisponivelGlobal.Value);
                    }

                    LogTxt(JsonConvert.SerializeObject(new { aumento, respostaLimiteCdt }));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(new LogEntity { Exception = ex }, new { message = "Erro ao carregar os ids" });
                throw;
            }
        }

        private IEnumerable<AumentoDeLimiteNeon> CarregarAumentos()
        {
            var lista = new List<AumentoDeLimiteNeon>();
            var linhas = File.ReadAllLines(Parametros.CAMINHO_ARQUIVO);

            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Client"))
                    continue;

                var valores = linhas[i].Split(';');

                var ajuste = new AumentoDeLimiteNeon
                {
                    IdCliente = int.Parse(valores[0]),
                    RequisicaoAumento = new ConductorRequisicaoAumentoDeLimite
                    {
                        NovoValorGlobal = decimal.Parse(valores[1], NumberStyles.AllowDecimalPoint, new CultureInfo("pt-BR")),
                        NovoValorLimiteMaximo = valores.Length > 2 ? decimal.Parse(valores[1], NumberStyles.AllowDecimalPoint, new CultureInfo("pt-BR")) : 0,
                    }
                };

                lista.Add(ajuste);
            }

            IncluirIdContaConductor(lista);

            return lista;
        }

        private void IncluirIdContaConductor(List<AumentoDeLimiteNeon> ajustes)
        {
            var clientesComIdContaConductor = _pottencialRepositorio.BuscarIdContaConductorCredito(ajustes.Select(x => x.IdCliente).ToArray());

            foreach (var ajuste in ajustes)
                ajuste.RequisicaoAumento.IdContaConductorCredito = clientesComIdContaConductor.FirstOrDefault(x => x.Idcliente == ajuste.IdCliente).IdContaConductorCredito;
        }

        private void LogTxt(string linha)
        {
            if (!Parametros.EH_PRA_VALER)
                return;

            _logger.Info(new LogEntity(), new { linha = linha });
            File.AppendAllLines(Parametros.CAMINHO_ARQUIVO_SUCESSO, new[] { linha });
        }
    }
}
