﻿using Microsoft.Extensions.Configuration;
using Neon.Domain.Interface.Helper;
using System;
using System.IO;
using System.Runtime.Caching;
using System.Text.Json;
using System.Threading;

namespace Neon.Domain.Helper
{
    public class Cache<T> : ICache<T>
    {
        private readonly string _directoryCache;

        public Cache(IConfiguration configuration)
        {
            _directoryCache = configuration.GetValue<string>("FolderCache");
        }

        public T Get(string key, TypeCache typeCache = TypeCache.MemoryCache)
        {
            if (typeCache == TypeCache.MemoryCache)
            {
                Thread.Sleep(500);
                return (T)MemoryCache.Default.Get(Path.Combine(_directoryCache, key));
            }
               

            CreateDirectory();

            if (File.Exists(Path.Combine(_directoryCache, key)))
                return JsonSerializer.Deserialize<T>(File.ReadAllText(Path.Combine(_directoryCache, key)));

            return default;
        }

        public void Set(string key, T value, int expiresIn = 3600, TypeCache typeCache = TypeCache.MemoryCache)
        {
            if (typeCache == TypeCache.MemoryCache)
                MemoryCache.Default.Set(key, value, new CacheItemPolicy() { AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddSeconds(expiresIn)) });
            else
            {
                CreateDirectory();
                File.WriteAllText(Path.Combine(_directoryCache, key), JsonSerializer.Serialize(value));
            }

        }

        public void Clear()
        {
            if (!Directory.Exists(_directoryCache))
                Directory.Delete(_directoryCache);
        }

        private void CreateDirectory()
        {
            if (!Directory.Exists(_directoryCache))
                Directory.CreateDirectory(_directoryCache);
        }
    }

    public enum TypeCache
    {
        MemoryCache = 1,
        File = 2
    }
}
