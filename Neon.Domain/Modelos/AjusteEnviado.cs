﻿namespace Neon.Domain.Modelos
{
    public class AjusteEnviado
    {
        public int IdCliente { get; set; }
        public ConductorRespostaAjuste RespostaAjuste { get; set; }
    }
}
