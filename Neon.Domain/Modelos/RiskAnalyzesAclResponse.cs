﻿using Newtonsoft.Json;

namespace Neon.Domain.Modelos
{
    public class RiskAnalyzesAclResponse
    {
        [JsonProperty("analyzeAction")]
        public int AnalyzeAction { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("proccessed")]
        public bool Proccessed { get; set; }
    }
}
