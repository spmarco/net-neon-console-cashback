﻿namespace Neon.Domain.Modelos
{
    public class AjusteNeon
    {
        public int IdCliente { get; set; }
        public ConductorRequisicaoAjuste RequisicaoAjuste { get; set; }
    }
}
