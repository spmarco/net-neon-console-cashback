﻿using Newtonsoft.Json;

namespace Neon.Domain.Modelos
{
    public class CreditSimulationRequest
    {
        public CreditSimulationRequest(string comercialPartnerCode,
                                       string document,
                                       int commercialBranchCode)
        {
            ComercialPartnerCode = comercialPartnerCode;
            Document = document;
            CommercialBranchCode = commercialBranchCode;
            FederalCode = "SP";
            CreditOperationCode = 1;
            VehicleGroupCode = 9;
            Channel = "NEON";
            UserLogin = "_neon";
            Ddd = string.Empty;
            Telephone = string.Empty;
        }

        [JsonProperty("codigoParceiroComercial")]
        public string ComercialPartnerCode { get; }

        [JsonProperty("numeroCpfCnpj")]
        public string Document { get; }

        [JsonProperty("codigoFilialComercial")]
        public int CommercialBranchCode { get; }

        [JsonProperty("codigoUnidadeFederacaoParceiroComercial")]
        public string FederalCode { get; }

        [JsonProperty("codigoOperacaoCredito")]
        public int CreditOperationCode { get; }

        [JsonProperty("codigoGrupoCategoriaVeiculo")]
        public int VehicleGroupCode { get; }

        [JsonProperty("canal")]
        public string Channel { get; }

        [JsonProperty("loginUsuario")]
        public string UserLogin { get; }

        [JsonProperty("ddd")]
        public string Ddd { get; }

        [JsonProperty("telefone")]
        public string Telephone { get; }
    }
}
