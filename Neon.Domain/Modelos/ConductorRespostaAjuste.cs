﻿using System;

namespace Neon.Domain.Modelos
{
    public class ConductorRespostaAjuste
    {
        public int Id { get; set; }
        public int IdTipoAjuste { get; set; }
        public int IdConta { get; set; }
        public DateTime DataAjuste { get; set; }
        public decimal Valor { get; set; }
        public object IdentificadorExterno { get; set; }
        public int Status { get; set; }
    }
}
