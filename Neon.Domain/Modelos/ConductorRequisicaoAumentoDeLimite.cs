﻿namespace Neon.Domain.Modelos
{
    public class ConductorRequisicaoAumentoDeLimite
    {
        public int IdContaConductorCredito { get; set; }
        public decimal NovoValorGlobal { get; set; }
        public decimal NovoValorLimiteMaximo { get; set; }
    }
}
