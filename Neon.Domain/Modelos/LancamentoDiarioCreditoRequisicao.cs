﻿using System;

namespace Neon.Domain.Modelos
{
    public class LancamentoDiarioCreditoRequisicao : LancamentoDiarioRequisicao
    {
        public LancamentoDiarioCreditoRequisicao(int idContaBancaria,
                                                 string tipoOperacao,
                                                 string tipoLancamento,
                                                 decimal valor,
                                                 int idCategoria,
                                                 DateTime dataTransacao,
                                                 DateTime dataCadastro,
                                                 string descricao,
                                                 string uniqueId,
                                                 string aplicar,
                                                 int status)
            : base(idContaBancaria,
                   tipoOperacao,
                   tipoLancamento,
                   valor,
                   idCategoria,
                   dataTransacao,
                   dataCadastro,
                   descricao,
                   uniqueId,
                   aplicar,
                   status)
        { }

        public DateTime? DataOriginalTransacao { get; set; }
    }
}
