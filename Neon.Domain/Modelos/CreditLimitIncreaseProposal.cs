﻿using System;

namespace Neon.Domain.Modelos
{
    public class CreditLimitIncreaseProposal
    {
        public int Id { get; private set; }
        public int ClientId { get; private set; }
        public decimal OldLimit { get; private set; }
        public decimal NewLimit { get; private set; }
        public decimal? NewLimitAccepted { get; private set; }
        public DateTime ExpirationDate { get; private set; }
        public DateTime ProposalDate { get; private set; }
        public DateTime? ProposalAcceptanceOrRefusalDate { get; private set; }
        public ProposalStatus Status { get; private set; }
    }
}
