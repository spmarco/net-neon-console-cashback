﻿
using Newtonsoft.Json;

namespace Neon.Domain.Modelos
{
    public class ConductorUpdateLimitAmountResponse
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "limiteGlobal")]
        public decimal globalLimit { get; set; }
        [JsonProperty(PropertyName = "limiteCompra")]
        public decimal PurchaseLimit { get; set; }
        [JsonProperty(PropertyName = "limiteParcelado")]
        public decimal InstallmentLimit { get; set; }
        [JsonProperty(PropertyName = "limiteParcelas")]
        public decimal InstallmentsLimit { get; set; }
        [JsonProperty(PropertyName = "limiteSaqueGlobal")]
        public decimal WithdrawGlobalLimit { get; set; }
        [JsonProperty(PropertyName = "limiteSaquePeriodo")]
        public decimal WithDrawPeriodLimit { get; set; }
        [JsonProperty(PropertyName = "limiteConsignado")]
        public decimal PayrollLimit { get; set; }
        [JsonProperty(PropertyName = "limiteInternacionalCompra")]
        public decimal InternationalPurchaseLimit { get; set; }
        [JsonProperty(PropertyName = "limiteInternacionalParcelado")]
        public decimal InternationalInstallmentLimit { get; set; }
        [JsonProperty(PropertyName = "limiteInternacionalParcelas")]
        public decimal InternationalInstallmentsLimit { get; set; }
        [JsonProperty(PropertyName = "limiteInternacionalSaqueGlobal")]
        public decimal InternationalWithdrawGlobalLimit { get; set; }
        [JsonProperty(PropertyName = "limiteInternacionalSaquePeriodo")]
        public decimal InternationalWithdrawPeriodLimit { get; set; }
        [JsonProperty(PropertyName = "limiteMaximo")]
        public decimal MaxLimit { get; set; }
        [JsonProperty(PropertyName = "saldoDisponivelGlobal")]
        public decimal BalanceAvaliableGlobal { get; set; }
        [JsonProperty(PropertyName = "saldoDisponivelCompra")]
        public decimal BalanceAvaliablePurchase { get; set; }
        [JsonProperty(PropertyName = "saldoDisponivelParcelado")]
        public decimal BalanceAvaliableInstallment { get; set; }
        [JsonProperty(PropertyName = "saldoDisponivelParcelas")]
        public decimal BalanceAvaliableInstallaments { get; set; }
        [JsonProperty(PropertyName = "saldoDisponivelSaque")]
        public decimal BalanceAvaliableWithdraw { get; set; }
        [JsonProperty(PropertyName = "saldoPontosFidelidade")]
        public decimal BalanceLoyaltyPoints { get; set; }
        [JsonProperty(PropertyName = "saldoDisponivelCompraInternacional")]
        public decimal BalanceAvaliableInternationalPurchase { get; set; }
        [JsonProperty(PropertyName = "saldoDisponivelSaqueInternacional")]
        public decimal BalanceAvaliableInternationalWithdraw { get; set; }
    }


    public class ConductorLimiteResposta
    {
        public int Id { get; set; }
        public decimal? LimiteGlobal { get; set; }
        public decimal? LimiteCompra { get; set; }
        public decimal? LimiteParcelado { get; set; }
        public decimal? LimiteParcelas { get; set; }
        public decimal? LimiteSaqueGlobal { get; set; }
        public decimal? LimiteSaquePeriodo { get; set; }
        public decimal? LimiteConsignado { get; set; }
        public decimal? LimiteInternacionalCompra { get; set; }
        public decimal? LimiteInternacionalParcelado { get; set; }
        public decimal? LimiteInternacionalParcelas { get; set; }
        public decimal? LimiteInternacionalSaqueGlobal { get; set; }
        public decimal? LimiteInternacionalSaquePeriodo { get; set; }
        public decimal? LimiteMaximo { get; set; }
        public decimal? SaldoDisponivelGlobal { get; set; }
        public decimal? SaldoDisponivelCompra { get; set; }
        public decimal? SaldoDisponivelParcelado { get; set; }
        public decimal? SaldoDisponivelParcelas { get; set; }
        public decimal? SaldoDisponivelSaque { get; set; }
        public decimal? SaldoPontosFidelidade { get; set; }
        public decimal? SaldoDisponivelCompraInternacional { get; set; }
        public decimal? SaldoDisponivelSaqueInternacional { get; set; }
    }



}
