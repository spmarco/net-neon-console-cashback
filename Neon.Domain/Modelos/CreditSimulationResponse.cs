﻿using Newtonsoft.Json;

namespace Neon.Domain.Modelos
{
    public class CreditSimulationResponse
    {
        public string Document { get; set; }

        [JsonProperty("codigoSimulacao")]
        public string SimulationCode { get; set; }

        [JsonProperty("indicadorRestricaoCredito")]
        public bool IndicatorCreditRestriction { get; set; }

        [JsonProperty("descricaoMotivoRestricaoCredito")]
        public string DescriptionCreditRestrictionReason { get; set; }

        [JsonProperty("situacaoPessoaCredito")]
        public CreditPersonSituationResponse CreditPersonSituation { get; set; }

        [JsonProperty("dadosPreAprovado")]
        public PreApprovalDataResponse PreApprovalData { get; set; }

        [JsonProperty("notaCliente")]
        public int ClientNote { get; set; }
    }

    public class CreditPersonSituationResponse
    {
        [JsonProperty("codigo")]
        public BvApiReponseCode Code { get; set; }

        [JsonProperty("descricao")]
        public string Description { get; set; }
    }

    public class PreApprovalDataResponse
    {
        [JsonProperty("codigo")]
        public int Code { get; set; }

        [JsonProperty("percentualMinimoValorEntrada")]
        public decimal MinimumPercentageEntryValue { get; set; }

        [JsonProperty("prazoMaximoFinanciamento")]
        public decimal MaximumFinancingDeadline { get; set; }

        [JsonProperty("valorMaximoFinanciamento")]
        public decimal MaximumFinancingValue { get; set; }

        [JsonProperty("pmtMaximo")]
        public decimal MaximumPmt { get; set; }
    }

    public enum BvApiReponseCode
    {
        Neutral,
        OperationImpeded,
        Restriction,
        PreApproved,
    }

    public class OAuthResponse
    {
        public string Access_Token { get; set; }
        public string Token_Type { get; set; }
        public int Expires_In { get; set; }
        public string Scope { get; set; }
    }
}
