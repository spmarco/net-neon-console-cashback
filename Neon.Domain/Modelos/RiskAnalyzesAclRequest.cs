﻿using Newtonsoft.Json;
using System;

namespace Neon.Domain.Modelos
{
    public class RiskAnalyzesAclRequest
    {
        [JsonProperty("clientId")]
        public int ClientId { get; private set; }
        [JsonProperty("requestGUID")]
        public string RequestGUID { get; private set; }

        [JsonProperty("clientRegisterId")]
        public int ClientRegisterId { get; private set; }

        [JsonProperty("analyseType")]
        public int AnalyseType { get; private set; }

        public RiskAnalyzesAclRequest(int clientId)
        {
            ClientId = clientId;
            RequestGUID = Guid.NewGuid().ToString();
            ClientRegisterId = 0;
            AnalyseType = 1;
        }
    }
}
