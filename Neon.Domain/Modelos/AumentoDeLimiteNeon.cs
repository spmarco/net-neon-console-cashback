﻿namespace Neon.Domain.Modelos
{
    public class AumentoDeLimiteNeon
    {
        public int IdCliente { get; set; }
        public ConductorRequisicaoAumentoDeLimite RequisicaoAumento { get; set; }
    }
}
