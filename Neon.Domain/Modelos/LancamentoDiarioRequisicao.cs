﻿using System;
using System.Threading.Tasks;

namespace Neon.Domain.Modelos
{
    public class LancamentoDiarioRequisicao
    {
        public LancamentoDiarioRequisicao(int idContaBancaria,
                                          string tipoOperacao,
                                          string tipoLancamento,
                                          decimal valor,
                                          int idCategoria,
                                          DateTime dataTransacao,
                                          DateTime dataCadastro,
                                          string descricao,
                                          string uniqueId,
                                          string aplicar,
                                          int status)
        {
            IdContaBancaria = idContaBancaria;
            TipoOperacao = tipoOperacao;
            TipoLancamento = tipoLancamento;
            Valor = valor;
            Descricao = descricao;
            IdCategoria = idCategoria;
            DataTransacao = dataTransacao;
            Status = status;
            DataCadastro = dataCadastro;
            UniqueId = uniqueId;
            Aplicar = aplicar;
        }

        public int Id { get; set; }
        public int IdContaBancaria { get; set; }
        public string TipoOperacao { get; set; }
        public string TipoLancamento { get; set; }
        public decimal Valor { get; set; }
        public string Descricao { get; set; }
        public int IdCategoria { get; set; }
        public DateTime DataTransacao { get; set; }
        public int Status { get; set; } = 1;
        public DateTime DataCadastro { get; set; }
        public string UniqueId { get; set; }
        public int CodigoMoeda { get; set; } = 986;
        public string Aplicar { get; set; }
        public int CodigoErro { get; set; }
        public string CodigoMensagem { get; set; }
        public string ObjetoJson { get; set; }

        internal void FallbackAsync(object executeFallBack, Func<object, Task> p)
        {
            throw new NotImplementedException();
        }
    }
}