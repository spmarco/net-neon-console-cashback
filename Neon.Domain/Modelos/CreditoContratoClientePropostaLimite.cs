﻿namespace Neon.Domain.Modelos
{
    public class CreditoContratoClientePropostaLimite
    {
        public int Id { get; set; }
        public int IdCliente { get; set; }
        public int IdContaConductorCredito { get; set; }
        public decimal LimiteAntigo { get; private set; }
        public decimal NovoLimite { get; private set; }
    }
}
