﻿using System;

namespace Neon.Domain.Modelos
{
    public class Transacao
    {
        public int id { get; set; }
        public int idTipoTransacao { get; set; }
        public string descricaoAbreviada { get; set; }
        public string statusTransacao { get; set; }
        public int idEvento { get; set; }
        public string tipoEvento { get; set; }
        public int idConta { get; set; }
        public string cartaoMascarado { get; set; }
        public string nomePortador { get; set; }
        public object dataTransacaoUTC { get; set; }
        public DateTime dataTransacao { get; set; }
        public DateTime? dataFaturamento { get; set; }
        public string dataVencimento { get; set; }
        public object modoEntradaTransacao { get; set; }
        public object valorTaxaEmbarque { get; set; }
        public object valorEntrada { get; set; }
        public float valorBRL { get; set; }
        public float? valorUSD { get; set; }
        public object cotacaoUSD { get; set; }
        public object dataCotacaoUSD { get; set; }
        public string codigoMoedaOrigem { get; set; }
        public string codigoMoedaDestino { get; set; }
        public string codigoAutorizacao { get; set; }
        public string codigoReferencia { get; set; }
        public string codigoTerminal { get; set; }
        public int? codigoMCC { get; set; }
        public int? grupoMCC { get; set; }
        public string grupoDescricaoMCC { get; set; }
        public int idEstabelecimento { get; set; }
        public string nomeEstabelecimento { get; set; }
        public string nomeFantasiaEstabelecimento { get; set; }
        public string localidadeEstabelecimento { get; set; }
        public int? planoParcelamento { get; set; }
        public int? numeroParcela { get; set; }
        public string detalhesTransacao { get; set; }
        public int? flagCredito { get; set; }
        public int? flagFaturado { get; set; }
        public object flagEstorno { get; set; }
        public object idTransacaoEstorno { get; set; }
    }

}
