﻿namespace Neon.Domain.Modelos
{
    public class Paginacao<T>
    {
        public int number { get; set; }
        public int size { get; set; }
        public int totalPages { get; set; }
        public int numberOfElements { get; set; }
        public int totalElements { get; set; }
        public bool firstPage { get; set; }
        public bool hasPreviousPage { get; set; }
        public bool hasNextPage { get; set; }
        public bool hasContent { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public int nextPage { get; set; }
        public int previousPage { get; set; }
        public T[] content { get; set; }
    }

}
