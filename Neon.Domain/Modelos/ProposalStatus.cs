﻿namespace Neon.Domain.Modelos
{
    public enum ProposalStatus
    {
        Pending = 1,
        Accepted = 2,
        Refused = 3,
        Expired = 4,
        DeclinedAccountBlocked = 5,
        DeclinedLateInvoice = 6,
        DeclinedOldLimitGreaterThanOld = 7
    }
}
