﻿namespace Neon.Domain.Modelos
{
    public class AjusteNeonResposta
    {
        public int IdCliente { get; set; }
        public ConductorRespostaAjuste Resposta { get; set; }
    }
}
