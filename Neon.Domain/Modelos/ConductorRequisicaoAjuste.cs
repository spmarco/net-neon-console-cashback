﻿using System;

namespace Neon.Domain.Modelos
{
    public class ConductorRequisicaoAjuste
    {
        public TipoAjusteConductor IdTipoAjuste { get; set; }
        public DateTime? DataAjuste { get; set; }
        public decimal ValorAjuste { get; set; }
        public int IdContaConductor { get; set; }
    }
}
