﻿using Flurl.Http;
using Microsoft.Extensions.Configuration;
using Neon.Domain.Helper;
using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Helper;
using Neon.Domain.Interface.Negocio.HttpCall;
using Neon.Domain.Modelos;
using Neon.Infrastructure.Abstractions.Logger.Extensions;
using Neon.Infrastructure.Abstractions.Logger.Interfaces;
using Polly;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using IConfigurationProvider = Neon.Infrastructure.Abstractions.Configuration.Interfaces.IConfigurationProvider;

namespace Neon.Domain.ACL
{
    public class BvAcl : IBvAcl
    {
        private string _baseUrl;
        private string _urlAuth;
        private string _urlRequestCredit;
        private string _clientBvId;
        private string _clientBvSecret;
        private string _partinerCommercialCode;
        private int _branchCode;
        private readonly ILog _logger;
        private readonly IHttpCallBase _httpCallBase;
        private readonly ICache<OAuthResponse> _cache;
        private readonly ICache<CreditSimulationResponse> _cacheCreditSimulation;
        private readonly IConfiguration _configuration;
        private static readonly object cacheLock = new object();

        public BvAcl(ILog logger,
                     IConfigurationProvider configurationProvider,
                     IHttpCallBase httpCallBase,
                     ICache<OAuthResponse> cache,
                     ICache<CreditSimulationResponse> cacheCreditSimulation,
                     IConfiguration configuration)
        {
            LoadDefaultsValues(configurationProvider);
            _logger = logger;
            _httpCallBase = httpCallBase;
            _cache = cache;
            _cacheCreditSimulation = cacheCreditSimulation;
            _configuration = configuration;
        }

        private void LoadDefaultsValues(IConfigurationProvider configurationProvider)
        {
            _baseUrl = configurationProvider.GetAs<string>("neon.bv.api.base.url");
            _urlAuth = configurationProvider.GetAs<string>("neon.bv.api.url.auth");
            _urlRequestCredit = configurationProvider.GetAs<string>("neon.bv.api.url.credit.request");

            _clientBvId = configurationProvider.GetAs<string>("neon.bv.api.client.id");
            _clientBvSecret = configurationProvider.GetAs<string>("neon.bv.api.client.secret");

            _partinerCommercialCode = configurationProvider.GetAs<string>("neon.bv.api.credit.partner.code");
            _branchCode = configurationProvider.GetAs<int>("neon.bv.api.credit.filial.code");
        }

        public OAuthResponse Authenticate()
        {
            var bodyRequest = $"client_id={_clientBvId}&client_secret={_clientBvSecret}&grant_type=client_credentials";

            var response = _httpCallBase.CallHttp((url) => url.WithHeader("Accept", "application/json, text/json, text/x-json, text/javascript, application/xml, text/xml")
                                                              .WithHeader("Content-Type", "application/x-www-form-urlencoded")
                                                              .PostStringAsync(bodyRequest)
                                                              .ReceiveJson<OAuthResponse>(),
                                                  _baseUrl,
                                                  _urlAuth).GetAwaiter().GetResult();

            return response.Result;
        }

        public async Task<CreditSimulationResponse> RequestCreditSimulationRetry(string document)
        {
            var cacheSimulation = _cacheCreditSimulation.Get(document, TypeCache.File);
            if (cacheSimulation != null)
                return cacheSimulation;

            var response = await Policy.HandleResult<CreditSimulationResponse>(x => x == null || x.ClientNote == -1)
                                       .WaitAndRetryAsync(_configuration.GetValue<int>("TimesToRetry"), retryAttempt => TimeSpan.FromSeconds(_configuration.GetValue<int>("SecondsRetryAttemptCall")),
                                       (retorno, timeSpan, retryCount, context) =>
                                       {
                                           _logger.Warning(new { Message = $"Error to call Simulation BV retry {retryCount}/4", document });
                                           _logger.Warning(JsonSerializer.Serialize(retorno));
                                       })
                                       .ExecuteAsync(async () => await RequestCreditSimulation(document));

            if (response == null || response.ClientNote == -1)
                throw new InvalidOperationException("RequestCreditSimulation return null or ClientNote == -1");

            response.Document = document;
            _cacheCreditSimulation.Set(document, response, typeCache: TypeCache.File);

            return response;
        }

        public async Task<CreditSimulationResponse> RequestCreditSimulation(string document)
        {
            var authorization = GetCachedOAuth();
            var header = new Dictionary<string, string>() { { "Authorization", $"Bearer {authorization.Access_Token}" } };
            var request = new CreditSimulationRequest(_partinerCommercialCode, document, _branchCode);

            var response = await _httpCallBase.CallHttpWithHeader((url) => url.PostJsonAsync(request)
                                                                              .ReceiveJson<CreditSimulationResponse>(),
                                                                  _baseUrl,
                                                                  _urlRequestCredit,
                                                                  header);

            return response.Result;
        }

        private OAuthResponse GetCachedOAuth()
        {
            var OAuthCache = _cache.Get("Neon.BV.Oauth");

            if (OAuthCache != null)
            {
                return OAuthCache;
            }

            lock (cacheLock)
            {
                if (OAuthCache != null)
                {
                    return OAuthCache;
                }

                OAuthCache = Authenticate();
                if (OAuthCache != null)
                {
                    _cache.Set("Neon.BV.Oauth", OAuthCache, Convert.ToInt32(OAuthCache.Expires_In / 2));
                    return OAuthCache;
                }
                return null;
            }
        }
    }
}
