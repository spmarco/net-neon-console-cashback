﻿using Flurl.Http;
using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Negocio.HttpCall;
using Neon.Domain.Modelos;
using Neon.Infrastructure.Abstractions.Configuration.Interfaces;
using Neon.Infrastructure.Abstractions.Statistics.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Neon.Domain.ACL
{
    public class ConductorPierACL : IConductorPierACL
    {
        private readonly string _baseUrl;
        private readonly string _accessToken;
        private readonly string _clientId;
        private readonly IStatisticProvider _statisticProvider;
        private readonly IHttpCallBase _httpCallBase;
        private readonly Dictionary<string, string> _header;

        public ConductorPierACL(IStatisticProvider statisticProvider, IConfigurationProvider configurationProvider, IHttpCallBase httpCallBase)
        {
            _baseUrl = configurationProvider.GetAs<string>("conductor.pier.url");
            _accessToken = configurationProvider.GetAs<string>("conductor.pier.token");
            _clientId = configurationProvider.GetAs<string>("conductor.pier.cliente");
            _statisticProvider = statisticProvider;
            _httpCallBase = httpCallBase;
            _header = GenerateHeader();
        }

        public ConductorRespostaAjuste EnviarAjusteFinanceiro(ConductorRequisicaoAjuste requisicao)
        {
            if (!Parametros.EH_PRA_VALER)
            {
                _statisticProvider.CountEvent("EnviarAjusteFinanceiro", JsonConvert.SerializeObject(requisicao), "sucess");
                return null;
            }

            var resposta = _httpCallBase.CallHttpWithHeader((url) => url.SetQueryParams(new
            {
                idTipoAjuste = (int)requisicao.IdTipoAjuste,
                dataAjuste = (requisicao.DataAjuste ?? DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ss.fffZ"),
                valorAjuste = requisicao.ValorAjuste.ToString(new CultureInfo("en-US")),
                idConta = requisicao.IdContaConductor
            })
                                                                        .PostJsonAsync(new { })
                                                                        .ReceiveJson<ConductorRespostaAjuste>(),
                                                            _baseUrl,
                                                            "/api/ajustes-financeiros",
                                                            _header).GetAwaiter().GetResult();

            return resposta.Result;
        }

        public bool AtivarContaCredito(int idContaConductorCredito)
        {
            if (!Parametros.EH_PRA_VALER)
            {
                _statisticProvider.CountEvent("AtivarContaCredito", idContaConductorCredito.ToString(), "sucess");
                return true;
            }

            var resposta = _httpCallBase.CallHttpWithHeader((url) => url.PostJsonAsync(new { }),
                                                            _baseUrl,
                                                            $"/api/contas/{idContaConductorCredito}/reativar",
                                                            _header).GetAwaiter().GetResult();

            return resposta.Success;
        }

        public Paginacao<Transacao> ObterTransacoesProcessadas(int idContaConductorCredito)
        {
            if (!Parametros.EH_PRA_VALER)
            {
                _statisticProvider.CountEvent("AtivarContaCredito", idContaConductorCredito.ToString(), "sucess");
                return null;
            }

            var result = _httpCallBase.CallHttpWithHeader((url) => url.AllowHttpStatus(HttpStatusCode.NotFound)
                                                                      .GetJsonAsync<Paginacao<Transacao>>(),
                                                          _baseUrl,
                                                          $"/api/contas/{idContaConductorCredito}/transacoes/listar-processadas",
                                                          _header).GetAwaiter().GetResult();

            return result.Result;
        }


        public ConductorLimiteResposta AlterarLImiteGlobalMaximo(int idContaConductorCredito, decimal limiteGlobal, decimal limiteMaximo)
        {
            if (!Parametros.EH_PRA_VALER)
            {
                _statisticProvider.CountEvent("AlterarLImiteGlobalMaximo", idContaConductorCredito.ToString(), "sucess");
                return null;
            }

            var result = _httpCallBase.CallHttpWithHeader((url) => url.SetQueryParams(new { idConta = idContaConductorCredito, limiteMaximo, limiteGlobal })
                                                                      .PutJsonAsync(new { })
                                                                      .ReceiveJson<ConductorLimiteResposta>(),
                                                           _baseUrl,
                                                           "/api/limites-disponibilidades",
                                                           _header,
                                                           false).GetAwaiter().GetResult();

            return result.Result;
        }

        public ConductorLimiteResposta AlterarLImiteGlobal(int idContaConductorCredito, decimal limiteGlobal)
        {
            if (!Parametros.EH_PRA_VALER)
            {
                _statisticProvider.CountEvent("AlterarLImiteGlobal", $"idContaConductorCredito:{idContaConductorCredito} | limiteGlobal:{limiteGlobal}", "sucess");
                return new ConductorLimiteResposta();

            }

            var result = _httpCallBase.CallHttpWithHeader((url) => url.SetQueryParams(new { idConta = idContaConductorCredito, limiteGlobal })
                                                                      .PutJsonAsync(new { })
                                                                      .ReceiveJson<ConductorLimiteResposta>(),
                                                           _baseUrl,
                                                           "/api/limites-disponibilidades",
                                                           _header,
                                                           false).GetAwaiter().GetResult();

            return result.Result;
        }


        public ConductorLimiteResposta ObterLimites(int idContaConductorCredito)
        {
            var result = _httpCallBase.CallHttpWithHeader((url) => url.SetQueryParams(new { idConta = idContaConductorCredito })
                                                                      .GetAsync()
                                                                      .ReceiveJson<ConductorLimiteResposta>(),
                                                           _baseUrl,
                                                           "/api/limites-disponibilidades",
                                                           _header,
                                                           false).GetAwaiter().GetResult();

            return result.Result;
        }

        protected Dictionary<string, string> GenerateHeader()
        {
            var header = new Dictionary<string, string>()
            {
                { "access_token", _accessToken },
                { "client_id", _clientId },
            };

            return header;
        }
    }
}
