﻿using Flurl.Http;
using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Negocio.HttpCall;
using Neon.Domain.Modelos;
using Neon.Infrastructure.Abstractions.Configuration.Interfaces;
using System;
using System.Threading.Tasks;

namespace Neon.Domain.ACL
{
    public class RiskAnalyzesAcl : IRiskAnalyzesAcl
    {
        private readonly IConfigurationProvider _configurationProvider;
        private readonly IHttpCallBase _httpCallBase;
        private string _baseUrl;
        private string _analizeUrl;

        public RiskAnalyzesAcl(IConfigurationProvider configurationProvider, IHttpCallBase httpCallBase)
        {
            _configurationProvider = configurationProvider;
            _httpCallBase = httpCallBase;
            _baseUrl = _configurationProvider.GetAs<string>("neon.microservice.approvalinternalrisk.internal");
            _analizeUrl = _configurationProvider.GetAs<string>("neon.microservice.approvalinternalrisk.internal.Analyzes");

            //_baseUrl = "http://localhost:5000/";
            //_analizeUrl = "v1/Analyzes/Analyze";
        }

        public async Task<RiskAnalyzesAclResponse> IsAFraudsterClient(RiskAnalyzesAclRequest request)
        {
            try
            {
                var resposta = await _httpCallBase.CallHttp((url) => url.PostJsonAsync(request)
                                                            .ReceiveJson<RiskAnalyzesAclResponse>(),
                                                     _baseUrl,
                                                     _analizeUrl,
                                                     request.ClientId);

                if (!resposta.Success)
                    throw resposta.Exception;

                return resposta.Result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
