﻿using Flurl.Http;
using Neon.Domain.Interface.Acl;
using Neon.Domain.Interface.Negocio.HttpCall;
using Neon.Domain.Modelos;
using Neon.Infrastructure.Abstractions.Configuration.Interfaces;
using Neon.Infrastructure.Abstractions.Statistics.Interfaces;
using Newtonsoft.Json;

namespace Neon.Domain.ACL
{
    public class LancamentoDiarioAcl : ILancamentoDiarioAcl
    {
        private readonly IStatisticProvider _statisticProvider;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly IHttpCallBase _httpCallBase;
        private string _baseUrl;

        public LancamentoDiarioAcl(IStatisticProvider statisticProvider, IConfigurationProvider configurationProvider, IHttpCallBase httpCallBase)
        {
            _statisticProvider = statisticProvider;
            _configurationProvider = configurationProvider;
            _httpCallBase = httpCallBase;
            SetBaseUrl();
        }


        public LancamentoDiarioRequisicao InserirLancamentoContaCorrente(LancamentoDiarioRequisicao requisicao)
        {
            if (!Parametros.EH_PRA_VALER)
            {
                _statisticProvider.CountEvent("InserirLancamentoContaCorrente", JsonConvert.SerializeObject(requisicao), "sucess");
                return null;
            }

            var resposta = _httpCallBase.CallHttp((url) => url.PostJsonAsync(requisicao)
                                                              .ReceiveJson<LancamentoDiarioRequisicao>(),
                                                        _baseUrl,
                                                        _configurationProvider.GetAs<string>("neon.microservico.lancamento.insert")).GetAwaiter().GetResult();


            return resposta.Result;
        }


        public LancamentoDiarioCreditoRequisicao InserirLancamentoCredito(LancamentoDiarioCreditoRequisicao requisicao)
        {
            if (!Parametros.EH_PRA_VALER)
            {
                _statisticProvider.CountEvent("InserirLancamentoCredito", JsonConvert.SerializeObject(requisicao), "sucess");
                return null;
            }

            var resposta = _httpCallBase.CallHttp((url) => url.PostJsonAsync(requisicao)
                                                              .ReceiveJson<LancamentoDiarioCreditoRequisicao>(),
                                                        _baseUrl,
                                                        "V1/LancamentoCredito/CadastrarLancamentoCreditoV2").GetAwaiter().GetResult();


            return resposta.Result;
        }

        private void SetBaseUrl()
        {
            _baseUrl = _configurationProvider.GetAs<string>("neon.microservico.lancamento");
        }
    }
}
